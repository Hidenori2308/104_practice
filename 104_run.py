import json
import requests
from bs4 import BeautifulSoup
import pandas as pd
from datetime import datetime as dt
import json
import time

job_title_ls = list()
loc_ls = list()
edu_deg_ls = list()
work_exp_ls = list()
com_name_ls = list()
com_addr_ls = list()
ind_cat_ls = list()
job_desc_ls = list()
man_resp_ls = list()
bus_trip_ls = list()
work_period_ls = list()
vac_policy_ls = list()
work_start_ls = list()
lang_req_ls = list()
skill_req_ls = list()
other_req_ls = list()
spec_req_ls = list()
sal_ls = list()
for page in range(1, 6):
    url = 'https://www.104.com.tw/jobs/search/?ro=0&kwop=7&keyword=Python&order=12&asc=0&page=%s&mode=s&jobsource=2018indexpoc' % (
        page)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36'

    }
    time.sleep(3)
    res = requests.get(url, headers=headers)
    # print(res.text)
    soup = BeautifulSoup(res.text, 'html.parser')

    for i in soup.select('a.js-job-link'):
        job_title_ls.append(i.text)
        job_id = i['href'].split('/')[-1].split('?')[0]
        json_url = 'https:' + i['href'].split(job_id)[0] + 'ajax/content/' + job_id
        headers = {'Referer': 'https:' + i['href']}
        response = requests.get(json_url, headers=headers)
        # print(response.text)
        jobDetail = json.loads(response.text)['data']['jobDetail']
        condition = json.loads(response.text)['data']['condition']
        lang_req = ''
        skill_req = ''
        spec_req = ''
        # 語言需求
        if len(condition['language']) != 0:
            for language in condition['language']:
                lang_req += language['language'] + '，' + language['ability'] + '。'
            lang_req_ls.append(lang_req)
        else:
            lang_req_ls.append('未註明')
        # 技能需求
        if len(condition['skill']) != 0:
            for skill in condition['skill']:
                skill_req += skill['description']
            skill_req_ls.append(skill_req)
        else:
            skill_req_ls.append(skill_req)
        # 擅長工具
        if len(condition['specialty']) != 0:
            for specialty in condition['specialty']:
                spec_req += specialty['description'] + '、'
            spec_req_ls.append(spec_req)
        else:
            spec_req_ls.append('未註明')
        other_req_ls.append(condition['other'] if len(condition['other']) != 0 else '未註明')  # 其他需求
        job_desc_ls.append(jobDetail['jobDescription'] if len(jobDetail['jobDescription']) != 0 else '未註明')  # 工作內容
        man_resp_ls.append(jobDetail['manageResp'] if len(jobDetail['manageResp']) != 0 else '未註明')  # 管理責任
        bus_trip_ls.append(jobDetail['businessTrip'] if len(jobDetail['businessTrip']) != 0 else '未註明')  # 出差外派
        work_period_ls.append(jobDetail['workPeriod'] if len(jobDetail['workPeriod']) != 0 else '未註明')  # 上班時段
        vac_policy_ls.append(jobDetail['vacationPolicy'] if len(jobDetail['vacationPolicy']) != 0 else '未註明')  # 休假制度
        work_start_ls.append(jobDetail['startWorkingDay'] if len(jobDetail['startWorkingDay']) != 0 else '未註明')  # 可上班日
        sal_ls.append(jobDetail['salary'] if len(jobDetail['salary']) != 0 else '未註明')  # 待遇
    # 公司名稱、公司地址、產業類別
    for i in soup.select('ul.b-list-inline.b-clearfix li a'):
        #     print(i)
        com_name_ls.append(i.text.strip())
        com_addr_ls.append(i['title'].replace('\n', ' ').split('公司住址：')[1])
        if i.findNext().findNext().text.strip()[-1] == '業':
            ind_cat_ls.append(i.findNext().findNext().text.strip())
        else:
            ind_cat_ls.append('未註明')

    # 地區、學歷、工作經驗
    # flag = all(map(lambda c:'\u4e00' <= c <= '\u9fa5',j))
    for i in soup.select('ul.b-list-inline.b-clearfix.job-list-intro.b-content li'):
        if len(i.text.strip()) == 0:
            continue
        if i.text.strip()[-1] == '區':
            loc_ls.append(i.text.strip())
        elif i.text.strip() in ['學歷不拘', '大學', '碩士', '專科']:
            edu_deg_ls.append(i.text.strip())
        else:
            work_exp_ls.append(i.text.strip())
    # print(len(job_title_ls),job_title_ls)
    # print(len(loc_ls),loc_ls)
    # print(len(loc_ls),com_name_ls)
    # print(len(work_exp_ls),work_exp_ls)
    # print(len(com_name_ls),com_name_ls)
    # print(len(com_addr_ls),com_addr_ls)
    # print(len(ind_cat_ls),ind_cat_ls)
    # print(len(edu_deg_ls),edu_deg_ls)

job_title = pd.Series(job_title_ls)
loc = pd.Series(loc_ls)
com_name = pd.Series(com_name_ls)
com_addr = pd.Series(com_addr_ls)
ind_cat = pd.Series(ind_cat_ls)
work_exp = pd.Series(work_exp_ls)
edu_deg = pd.Series(edu_deg_ls)
job_desc = pd.Series(job_desc_ls)  # 工作內容
man_resp = pd.Series(man_resp_ls)  # 管理責任
bus_trip = pd.Series(man_resp_ls)  # 出差外派
work_period = pd.Series(man_resp_ls)  # 上班時段
vac_policy = pd.Series(man_resp_ls)  # 休假制度
work_start = pd.Series(man_resp_ls)  # 可上班日
sal = pd.Series(sal_ls)  # 待遇
lang_req = pd.Series(lang_req_ls)  # 語言需求
skill_req = pd.Series(skill_req_ls)  # 技能需求
spec_req = pd.Series(spec_req_ls)  # 工具需求
other_req = pd.Series(other_req_ls)  # 其他需求
df = pd.DataFrame({'職稱': job_title,
                   '地區': loc,
                   '公司名稱': com_name,
                   '公司地址': com_addr,
                   '產業類別': ind_cat,
                   '工作經驗': work_exp,
                   '學歷': edu_deg,
                   '工作內容': job_desc,
                   '管理責任': man_resp,
                   '出差外派': bus_trip,
                   '上班時段': work_period,
                   '休假制度': vac_policy,
                   '可上班日': work_start,
                   '待遇': sal,
                   '語言需求': lang_req,
                   '技能需求': skill_req,
                   '工具需求': spec_req,
                   '其他需求': other_req
                   })
# display(df)
start = dt.now()
print('start...writing file')
df.to_csv('./104_{}.csv'.format(dt.now().strftime('%Y%m%d')), index=False, encoding='utf-8-sig')
end = dt.now()
print('spent {} s'.format((end - start).total_seconds()))